//
//  Analyser.m
//  Analyser
//
//  Created by Admin on 07.05.16.
//  Copyright © 2016 SmallTownGayBar. All rights reserved.
//

#import "Analyser.h"


@interface Analyser()
@property(nonatomic, strong) NSMutableDictionary *statistics;
@end

@implementation Analyser

- (id) init{
    self = [super init];
    if (self)
    {
        self.statistics = [[NSMutableDictionary alloc] init];
    }
    return self;
}

- (void) countAndPrint: (NSString*)text
{
    NSArray *words = [text componentsSeparatedByString:@" "];
    for (NSString* word in words)
    {
        NSNumber *repetitions= [self.statistics valueForKey: word];
        if ([repetitions integerValue] == 0)
        {
            [self.statistics setObject:[[NSNumber alloc] initWithLong:1] forKey:word];
        } else
        {
            [self.statistics setObject:[[NSNumber alloc] initWithLong:([repetitions integerValue] + 1)]  forKey:word];
        }
    }
    
    NSArray* sortedValues = [self.statistics keysSortedByValueUsingComparator:^(id obj1, id obj2) {
        int v1 = [obj1 intValue];
        int v2 = [obj2 intValue];
        if (v1 > v2)
            return NSOrderedAscending;
        else if (v1 < v2)
            return NSOrderedDescending;
        else
            return NSOrderedSame;
    }];
    
    NSArray *valueKeys = [self.statistics allValues];
    NSArray *sortedKeys = [valueKeys sortedArrayUsingSelector:@selector(compare:)];
    NSUInteger k = [sortedKeys count];
    
    NSUInteger count;
    
    if(k < 5){
        count = k;
    } else if (k >= 5){
        count = 5;
    }
    
    for (int i = 0 ; i < count	; i++) {
        NSLog(@"%@ %@", sortedValues[i], sortedKeys[k - i - 1]);
    }
}

@end
