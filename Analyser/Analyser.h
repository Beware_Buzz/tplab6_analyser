//
//  Analyser.h
//  Analyser
//
//  Created by Admin on 07.05.16.
//  Copyright © 2016 SmallTownGayBar. All rights reserved.
//


#import <Foundation/Foundation.h>

@interface Analyser : NSObject

- (void) countAndPrint: (NSString*)text;

@end